<?php

namespace App\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Routing\ClassResourceInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use FOS\RestBundle\Controller\Annotations;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Class WelcomeController
 * @package App\Controller
 * @RouteResource("login", pluralize=false)
 * @Security("has_role('ROLE_ADMIN')")
 */
class WelcomeController extends FOSRestController implements ClassResourceInterface
{
    /**
     * @Annotations\Get("/welcome")
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request)
    {
        return new JsonResponse(
            ['message' => 'random try'],
            JsonResponse::HTTP_OK
        );
    }
}