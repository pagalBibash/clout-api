<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Routing\ClassResourceInterface;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

use App\Entity\User;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;

/**
 * Class LoginController
 * @package App\Controller
 * @RouteResource("login", pluralize=false)
 */

class LoginController extends FOSRestController implements ClassResourceInterface
{
    public function __construct()
    {

    }

    /**
     * @Annotations\Post("/login")
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function loginAction(Request $request)
    {
        $username = $request->request->get('username');
        $password = $request->request->get('password');

        $user = $this->getDoctrine()
            ->getRepository(User::class)
            ->findOneBy(['username' => $username]);

        if(!$user) {
            throw $this->createNotFoundException();
        }

        $isValid = $this->get('security.password_encoder')
            ->isPasswordValid($user, $password);

        if (!$isValid) {
            throw new BadCredentialsException();
        }
        $token = $this->getToken($user);

        return new JsonResponse(
            [
                'data' => $user->getPublicInfo(),
                'token' => $token
            ],
            JsonResponse::HTTP_OK
        );
    }

    /**
     * Returns token for user.
     *
     * @param User $user
     *
     * @return array
     * @throws \Exception
     */
    public function getToken(User $user)
    {
        return $this->container->get('lexik_jwt_authentication.encoder')
            ->encode([
                'username' => $user->getUsername(),
                'exp' => $this->getTokenExpiryDateTime()
            ]);
    }

    /**
     * Returns token expiration datetime.
     *
     * @return string Unixtmestamp
     *
     * @throws \Exception
     */
    private function getTokenExpiryDateTime()
    {
        $tokenTtl = $this->container->getParameter('lexik_jwt_authentication.token_ttl');
        $now = new \DateTime();
        $now->add(new \DateInterval('PT'.$tokenTtl.'S'));
        return $now->format('U');
    }
}